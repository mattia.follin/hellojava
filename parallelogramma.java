/**
 * parallelogramma
 */
public class parallelogramma {

    private int dMag;
    private int dMin;
    private int altezza;
    private int perimetro;
    private int area;

    public int Perimetro() {
        perimetro = dMag*2 + dMin*2;
        return perimetro;
    }

    public int Area() {
        area = dMag * altezza;
        return area;
    }

    public parallelogramma(int dMag,int dMin,int altezza){
        super();
        this.dMag = dMag;
        this.dMin = dMin;
        this.altezza = altezza;
    }
}